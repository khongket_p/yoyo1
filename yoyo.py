#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import pandasql


# In[2]:


url = 'https://docs.google.com/spreadsheets/d/18_ExNkMP1_8uyLYs-SzXFnZZEOkjw72UJSi8NZud-GA/edit?usp=sharing'


# In[3]:


#!pip install oauth2client gspread


# In[4]:


from oauth2client.service_account import ServiceAccountCredentials
import gspread


# In[5]:


scope = ['https://www.googleapis.com/auth/spreadsheets']
credentials = ServiceAccountCredentials.from_json_keyfile_name('my_json_file.json', scope)
client = gspread.authorize(credentials)
sheet = client.open_by_url(url)
worksheet = sheet.get_worksheet(0)
results = worksheet.get_all_records()


# In[6]:


result_df = pd.DataFrame(results)


# In[7]:


result_df.columns


# In[8]:


len(result_df.columns) 


# In[9]:


result_df.shape


# In[10]:


result_df['ที่อยู่อีเมล'] = result_df['ที่อยู่อีเมล'].fillna('').apply(lambda x: x.strip().lower())


# In[11]:


qdf =  pandasql.sqldf("select ที่อยู่อีเมล from result_df group by ที่อยู่อีเมล")

qdf.head(20)


# In[12]:


result_df.head()


# In[13]:


result_df.columns.to_list()


# In[14]:


column_name = result_df.columns.to_list()


# In[15]:


column_name


# In[16]:


result_df.loc[result_df.สินค้าส่วนใหญ่ที่เลือกซื้อ == 'หูฟัง แป้นพิมพ์', 'สินค้าส่วนใหญ่ที่เลือกซื้อ'] = 'หูฟังแป้นพิมพ์'
result_df.loc[result_df.สินค้าส่วนใหญ่ที่เลือกซื้อ == 'อัลบั้ม', 'สินค้าส่วนใหญ่ที่เลือกซื้อ'] = 'อัลบั้มเพลง'


# In[17]:


result_df.loc[result_df.สินค้าส่วนใหญ่ที่เลือกซื้อ == 'ของใช้ที่จำเป็น,โปสเตอร์,สติ๊กเกอร์,อัลบั้ม,เสื้อผ้า', 'สินค้าส่วนใหญ่ที่เลือกซื้อ'] = 'ของใช้จำเป็น'


# In[18]:


qdf =  pandasql.sqldf("select สินค้าส่วนใหญ่ที่เลือกซื้อ from result_df group by สินค้าส่วนใหญ่ที่เลือกซื้อ")

qdf.head(20)


# In[19]:


q1 = "ของใช้จำเป็น"
q2 = "ชานมไข่มุก"
q3 = "สกินแคร์"
q4 = "หูฟังแป้นพิมพ์"
q5 = "อัลบั้มเพลง"
q6 = "อาหาร"
q7 = "เครื่องประดับ"
q8 = "เครื่องสำอาง"
q9 = "เสื้อผ้า"


# In[20]:


sql = "select สินค้าส่วนใหญ่ที่เลือกซื้อ, %s, %s, %s, %s, %s, %s, %s, %s, %s, from result_df group by สินค้าส่วนใหญ่ที่เลือกซื้อ" % (q1, q2, q3, q4, q5, q6, q7, q8, q9)


# In[21]:


sql


# In[24]:


result_df.to_csv('result.csv', encoding='utf8', columns = column_name, index=False)


# In[23]:


qdf.to_csv('report', encoding='utf8', index=False)


# In[ ]:




